import pyperclip
import helium as hl
import json
from os import chdir

def courseURL(course:str, episode:int) -> str:
    """
    Crawls code from codewithharry website.
    """
    with open("C:\\Users\\Admin\\Documents\\cwh-code-downloader\\courses.json") as f:
        fc = f.read()
    parser = json.loads(fc)

    course_url, max = parser[course]["base_url"], parser[course]["max"]

    if episode > max:
        print("The last episode of the {} is {}.".format(course, max))

    return course_url.replace(":episode_no:", str(episode))

def copyCode(filename:str, url:str) -> None:
    hl.start_chrome(url)
    i = 1
    while True:
        try:
            print("Try {}".format(i+1))
            hl.click(hl.Button("Copy", below="Code as described/written in the video"))
            if len(pyperclip.paste() > 100):
                saveCode(filename, pyperclip.paste())
        except Exception as e:
            print("Try {}".format(i+1))
            print(e)
            i += 1

def saveCode(filename:str, code:str) -> None:
    with open(filename, 'w') as f:
        f.write(code)

if __name__ == "__main__":
    course = input("Which course? ")
    episode = int(input("Which episode? "))
    filename = input("Filename? ")

    if course == "Web Dev":
        subfolder = int(input("HTML CSS or JS? "))
        if subfolder == 1:
            chdir("C:\\Users\\Admin\\Documents\\Web-Dev-Notes\\html")
        elif subfolder == 2:
            chdir("C:\\Users\\Admin\\Documents\\Web-Dev-Notes\\css")
        elif subfolder == 3:
            chdir("C:\\Users\\Admin\\Documents\\Web-Dev-Notes\\js")
        else:
            print("Invalid selection")

        copyCode(filename, courseURL(course, episode))

    elif course == "Flask":
        chdir("C:\\Users\\Admin\\Documents\\Web-Dev-Notes\\flask")
        copyCode(filename, courseURL(course, episode))

    elif course == "Django":
        chdir("C:\\Users\\Admin\\Documents\\Web-Dev-Notes\\django")
        copyCode(filename, courseURL(course, episode))

    else:
        print("Invalid input.")